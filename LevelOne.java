/**
 * LevelOne
 */
public class LevelOne {
    
    public static void main(String[] args) {
        int horizontal = Integer.parseInt(args[0]);
        int vertical = Integer.parseInt(args[1]);

        if (horizontal == vertical) {
            int[][] randomValues = new int[horizontal][vertical];
            int[] minValues = new int[horizontal];
            int[] maxValues = new int[horizontal];
            int[] averages = new int[horizontal];
            //
            int maxValue = 0;
            int minValue = 100;
            int gemiddelde = 0;

            // -- TOP -- //
            for (int horNumb = 0; horNumb < horizontal; horNumb++) {
               if (horNumb == 0) {
                    System.out.print("\t" + horNumb); 
                } else if (horNumb == horizontal -1) {
                    System.out.print("\t" + horNumb + "\t gemid.:");
                } else {
                    System.out.print("\t" + horNumb);
                }
            }

            System.out.print("\n\n");

            // -- CENTER -- //
            for (int horNumb = 0; horNumb < horizontal; horNumb++) {
                System.out.print(horNumb + "\t");
                for (int vertNumb = 0; vertNumb < vertical; vertNumb++) {
                    // generate a number between 1 and 100
                    int randomNum = (int)(Math.random() * 101);
                    System.out.print(randomNum + "\t");
                    gemiddelde = gemiddelde + randomNum;
                    randomValues[vertNumb][horNumb] = randomNum; 
                    if (vertNumb == vertical-1) {
                        gemiddelde = gemiddelde / vertical;
                        averages[horNumb] = gemiddelde;
                        System.out.print(gemiddelde + "\t");
                        System.out.print("\n");
                    }
                }
            }

            System.out.print("\n");
            // -- BOTTOM -- //
            for (int i = 0; i < randomValues.length; i++) {
                for (int value = 0; value < randomValues[i].length; value++) {
                    maxValue = randomValues[i][value] > maxValue ? randomValues[i][value] : maxValue;
                    minValue = randomValues[i][value] < minValue ? randomValues[i][value] : minValue;
                    if (value == randomValues[i].length-1){
                        maxValues[i] = maxValue;
                        minValues[i] = minValue;
                        maxValue = 0;
                        minValue = 100;  
                    }
                }
            }
            // all min values
            for (int i = 0; i < minValues.length; i++) {
                if (i == 0){
                    System.out.print("min: \t");
                } 
                System.out.print(minValues[i] + "\t");
                if (i == minValues.length-1){
                    System.out.print(getAverage(averages));
                }
            }

            System.out.print("\n");
            // all max values
            for (int i = 0; i < maxValues.length; i++) {
                if (i == 0){
                    System.out.print("max: \t");
                } 
                System.out.print(maxValues[i] + "\t");
                if (i == minValues.length-1){
                    System.out.print(getDeviation(averages));
                }
            }

        } else {
            System.out.println("Argument numbers do not match! please give 2 identical args, like: 4:4 or 10:10");
        }
    }

    // get the average of all averages - row = min
    static int getAverage(int[] averages){
        int average = 0;
        for (int randomAverage = 0; randomAverage < averages.length; randomAverage++) {
            average = average + averages[randomAverage];
        }
        average = average / averages.length;
        return average;
    }

    // get the deviation of all bottom values - row = max
    // https://examples.yourdictionary.com/examples-of-standard-deviation.html
    static double getDeviation(int[] averages) {
        double deviation = 0.00;
        int mean = 0;
        int averageTotal = 0;
        double[] squaredAverages = new double [averages.length];

        for (int randomAverage = 0; randomAverage < averages.length; randomAverage++) {
            averageTotal = averageTotal + averages[randomAverage];
            // System.out.println(averageTotal);
            mean = mean + averages[randomAverage];
        }
        // calculate the mean
        mean = mean / averages.length;
        // substract the mean from each value & square those differences - save this number in the squaredAverages array.
        for (int subMeans = 0; subMeans < averages.length; subMeans++) {
            averages[subMeans] = averages[subMeans] - mean;
            averages[subMeans] = averages[subMeans] * averages[subMeans];
            deviation = deviation + averages[subMeans];
        }

        deviation = deviation / squaredAverages.length;
        deviation = Math.sqrt(deviation);
        deviation = Math.round(deviation * 1000d) / 1000d;
        return deviation;
    }
}

